package com.testNGDataproviders.test;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import org.testng.annotations.Test;

public class DataProviderTest {

	protected WebDriver driver;

	final String USERNAME = "Rajahamsa";
	final String ACCESS_KEY = "756b4cfe-b3be-4c9b-9456-516af6af8c02";
	final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";

	protected WebDriver getDriverInstance() throws Exception {

		DesiredCapabilities caps = DesiredCapabilities.chrome();
		caps.setCapability("platform", "Windows 10");
		caps.setCapability("version", "60.0");
		caps.setCapability("name", "testNGDataproviders");

		driver = new RemoteWebDriver(new URL(URL), caps);
		return driver;
	}

	@BeforeMethod
	public void setUp() throws Exception {
		driver = getDriverInstance();
		driver.get("http://www.google.com");
	}

	@DataProvider(name = "searchFor")

	public static Object[][] credentials() {

		return new Object[][] { { "iphone8" }, { "iphoneSE" } };

	}

	@Test(dataProvider = "searchFor")

	public void search(String searchFor) {

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		WebElement search = driver.findElement(By.name("q"));
		search.sendKeys(searchFor);
		WebElement button = driver
				.findElement(By.xpath("//*[@id=\"sbtc\"]/div[2]/div[2]/div[1]/div/ul/li[11]/div/span[1]/span/input"));
		button.click();

	}
	
	@AfterMethod
	public void afterTest() {
		driver.quit();

	}

}